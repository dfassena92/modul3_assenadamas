import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Hello World',
      home: Scaffold(
        appBar: AppBar(
          title: Text('AssenaDamas'),
        ),
        body: Column(children: <Widget>[
          Container(
            margin: EdgeInsets.all(10.5),
            child: RaisedButton(
              child: Text('Add Product'),
              onPressed: () {},
            ),
          ),
          Card(
            child: Column(
              children: <Widget>[
                Image.asset('assets/food.jpg'),
                Text('Food Paradise'),
              ],
            ),
          ),
        ]),
      ),
    );
  }
}
