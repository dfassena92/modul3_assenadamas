import 'package:flutter/material.dart';
import './product_manager.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        brightness: Brightness.light,
        primarySwatch: Colors.deepOrange),
      title: 'Hello World',
      home: Scaffold(
        appBar: AppBar(
          title: Text('AssenaDamas'),
        ),
        body: ProductManager('Food Tester'),
      ),
    );
  }
}
