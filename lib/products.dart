import 'package:flutter/material.dart';

class Products extends StatelessWidget {
  List<String> products;
  Products(this.products) {
    print('[Product Widget] Constuctor');
  }

  @override
  Widget build(BuildContext context) {
    print('[Product Widget] build()');

    return Column(
      children: products
          .map(
            (el) => Card(
                  child: Column(
                    children: <Widget>[
                      Image.asset('assets/food.jpg'),
                      Text(el),
                    ],
                  ),
                ),
          )
          .toList(),
    );
  }
}
